#!/bin/bash

# Default variables
hostnames_file=""
source_directories=()
destination_directories=()

# Help function
function show_help {
    echo "Usage: $0 -h <hostnames_file> -s <source_directory1>,<source_directory2> -d <destination_directory1>,<destination_directory2>"
    echo "Arguments:"
    echo "  -h <hostnames_file>:      File containing the list of server hostnames"
    echo "  -s <source_directory>:    Source directory(s) to copy, separated by comma"
    echo "  -d <destination_directory>: Destination directory(s) on the servers, separated by comma"
}

# Process command-line arguments
while getopts ":h:s:d:" opt; do
    case $opt in
        h)
            hostnames_file=$OPTARG
            ;;
        s)
            IFS=',' read -ra source_directories <<< "$OPTARG"
            ;;
        d)
            IFS=',' read -ra destination_directories <<< "$OPTARG"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            show_help
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            show_help
            exit 1
            ;;
    esac
done

# Check if required arguments are provided
if [[ -z $hostnames_file || ${#source_directories[@]} -eq 0 || ${#destination_directories[@]} -eq 0 ]]; then
    echo "Missing required arguments."
    show_help
    exit 1
fi

# Check if hostnames file exists
if [[ ! -f "$hostnames_file" ]]; then
    echo "The file $hostnames_file does not exist."
    exit 1
fi

# Prompt for username and password
read -p "Username: " username
read -s -p "Password: " password
echo

# Function to copy a directory to a server
function copy_directory {
    hostname=$1
    username=$2
    password=$3
    source_directory=$4
    destination_directory=$5

    # Copy directory using scp
    sshpass -p "$password" scp -o StrictHostKeyChecking=no -r "$source_directory" "$username"@"$hostname":"$destination_directory"
}

# Iterate over the servers in the hostnames file
while IFS= read -r line || [[ -n "$line" ]]; do
    hostname=$(echo "$line" | sed 's/^[[:space:]]*//;s/[[:space:]]*$//;s/^"//;s/"$//')
    for ((i=0; i<${#source_directories[@]}; i++)); do
        source_directory=${source_directories[$i]}
        destination_directory=${destination_directories[$i]}
        copy_directory "$hostname" "$username" "$password" "$source_directory" "$destination_directory"
    done
done < "$hostnames_file"

echo "Directory copy completed successfully!"