# MultiScp
this multiscp tool allows you to do scp to multiple host copying multiple directories with all its content

## requierements to work 
ssh pass to work

installation for mac: 

```
curl -L https://raw.githubusercontent.com/kadwanev/bigboybrew/master/Library/Formula/sshpass.rb > sshpass.rb && brew install sshpass.rb && rm sshpass.rb
´´´


syntax:

```
sh scp_multiple_hosts.sh  -h <hostnames_File> -s "<source_dir1>","<source_dir2>" -d "<destiny_dir1>","<destiny_dir2>"

´´´

Arguments:
  -h <hostnames_file>:      File containing the list of server hostnames
  -s <source_directory>:    Source directory(s) to copy, separated by comma
  -d <destination_directory>: Destination directory(s) on the servers, separated by comma

se hostnames_examples.txt structure for input

example :

```
sh scp_multiple_hosts.sh  -h hostnames.txt -s "./source","./source2" -d "Desktop\remote1","Desktop\remote2"
´´´

once user is prompted, if you use this script from linux to windows and if  it uses "\" character make sure to include it twice as follows:

username: DOMAIN\\RSGARCIAE

